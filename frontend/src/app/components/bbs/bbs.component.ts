import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { HttpHeaders } from '@angular/common/http'

interface Bbs {
  id: number;
  user_id: number;
  date: number;
  title: string;
  content: string;
};

@Component({
  selector: 'app-bbs',
  templateUrl: './bbs.component.html',
  styleUrls: ['./bbs.component.css']
})
export class BbsComponent implements OnInit {
  private host: string = environment.serverUrl;
  results: Bbs[] = [];

  private httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    })
   };

  bbsForm = new FormGroup({
    userId: new FormControl('', [
      Validators.required
    ]),
    title: new FormControl('', [
      Validators.required
    ]),
    content: new FormControl('', [
      Validators.required
    ])
  })

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(`${this.host}/bbs`, this.httpOptions).subscribe(
      (res: any) => {
        this.results = res;
      }
    )
  }

  get userId() {
    return this.bbsForm.get('userId');
  }

  get title() {
    return this.bbsForm.get('title');
  }

  get content() {
    return this.bbsForm.get('content');
  }

  timestampToDate(timestamp: number): string {
    return (new Date(timestamp)).toString();
  }

  onSubmit() {
    let bbsData = {'id': 0, 'user_id': Number(this.userId?.value), 'title': this.title?.value, 'content': this.content?.value}
    //const body = JSON.stringify(bbsData)
    this.http.post<any>(`${this.host}/bbs`, bbsData, this.httpOptions).subscribe(data => {
      if (!!data) {
        this.ngOnInit()
      }
    })
    this.userId?.setValue('');
    this.title?.setValue('');
    this.content?.setValue('');
  }
}
