-- MySQL dump 10.13  Distrib 8.0.25, for macos11.3 (x86_64)
--
-- Host: localhost    Database: sample
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

use `sample`

create user `osaki`@`localhost` IDENTIFIED BY '1192';

grant all privileges on sample.* to `osaki`@`localhost`;

--
-- Table structure for table `bbs`
--

DROP TABLE IF EXISTS `bbs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bbs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `date` datetime DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bbs`
--

LOCK TABLES `bbs` WRITE;
/*!40000 ALTER TABLE `bbs` DISABLE KEYS */;
INSERT INTO `bbs` VALUES (1,33,'2021-04-21 00:00:00','first bss','hello world'),(2,34,'2021-04-21 00:00:00','Play framework','studying play framework'),(3,35,'2021-04-21 00:00:00','angular framework','studying angular framework'),(4,3,'2021-04-23 12:30:53','first','content'),(5,100,'2021-04-23 14:22:16','','content'),(10,3,'2021-04-23 14:38:29','osaki','content'),(25,22,'2021-04-23 15:31:27','3333','333'),(26,22,'2021-04-23 15:31:29','3333','333'),(27,22,'2021-04-23 15:31:29','3333','333'),(28,22,'2021-04-23 15:31:29','3333','333'),(29,22,'2021-04-23 15:31:29','3333','333'),(30,22,'2021-04-23 15:31:30','3333','333'),(31,22,'2021-04-23 15:31:30','3333','333'),(32,22,'2021-04-23 15:31:30','3333','333'),(33,22,'2021-04-23 15:31:30','3333','333'),(34,22,'2021-04-23 15:31:30','3333','333'),(35,22,'2021-04-23 15:31:30','3333','333'),(36,22,'2021-04-23 15:31:30','3333','333'),(37,22,'2021-04-23 15:31:31','3333','333'),(38,33,'2021-04-23 15:33:02','22','33'),(39,22,'2021-04-23 15:33:40','22','3432'),(40,11111,'2021-04-23 15:36:09','hello','update'),(41,33,'2021-04-23 15:38:06','222','we'),(42,3333,'2021-04-23 15:38:45','222','33'),(43,33,'2021-04-23 15:39:09','22','33'),(44,3,'2021-04-26 10:30:39','new','helloworld'),(45,22,'2021-04-26 11:54:06','\"new\"','\"hellow\"'),(46,22,'2021-04-26 11:56:16','\"new\"','\"Hello\"'),(47,22,'2021-04-26 11:56:43','\"new\"','\"Hello\"'),(48,22,'2021-04-26 11:59:30','\"new\"','\"hello\"'),(49,22,'2021-04-26 12:03:44','new','hello'),(50,3,'2021-05-10 11:34:03','hello','osaki'),(51,3,'2021-05-10 11:34:57','掲示板','tarou'),(52,333,'2021-05-10 11:36:19','newcontent','helloworld'),(53,33,'2021-05-10 12:32:23','33','22'),(54,1000,'2021-05-10 16:14:48','osaki','modify'),(55,333,'2021-05-10 17:27:05','hello','mynameisosaki'),(56,2,'2021-05-11 17:25:24','3','2'),(57,333,'2021-05-11 17:25:39','hello','world'),(58,22,'2021-05-11 17:30:46','hello','osaki'),(59,1001,'2021-05-11 17:32:02','modify ','form'),(60,22,'2021-09-02 16:24:27','lslslslslslslslosakijsjjs','testtest'),(61,22,'2021-09-02 16:39:16','new','Hello'),(62,3333,'2021-09-02 17:04:09','AfterModification','helloworldhelloworld'),(63,3,'2021-09-02 17:04:50','3','3');
/*!40000 ALTER TABLE `bbs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `City`
--

DROP TABLE IF EXISTS `City`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `City` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `City`
--

LOCK TABLES `City` WRITE;
/*!40000 ALTER TABLE `City` DISABLE KEYS */;
INSERT INTO `City` VALUES (2,'Combrdge','New Zealand'),(3,'Combrdge','New Zealand');
/*!40000 ALTER TABLE `City` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bbs_id` int NOT NULL,
  `user_id` int NOT NULL,
  `comment_id` varchar(100) NOT NULL,
  `comment_content` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,1,44,'0273c6d3-2cd9-42cf-8db6-f264514a645b','jrllo','2021-05-10 17:21:03'),(2,2,33,'89732c6a-ef0b-4c7d-951e-b62dfa88410c','first','2021-05-10 17:22:26'),(3,1,33,'5d0ce62f-b007-47ad-bec8-a060374b73f4','modify','2021-05-11 17:35:11'),(4,1,32,'e92fe236-4b8f-4fd6-837f-0bfa062361e3','modifysels','2021-05-11 17:36:20'),(5,1,22,'6e9dce6e-30b9-49bc-9a96-74e9f8491b9d','sample test','2021-09-02 16:41:19'),(6,1,3,'e4cf19fe-bc80-4029-a1e0-e50f036f5ddb','3','2021-09-02 17:11:44'),(7,33,333,'02997926-f8a8-47d4-84eb-8a385508ba5b','helllowowor','2021-09-02 17:13:18');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-10 10:08:00
