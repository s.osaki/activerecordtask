package models

import com.github.aselab.activerecord._
import com.github.aselab.activerecord.dsl._
import play.api.libs.json._
import java.sql.Timestamp

case class Bbs(override val id: Long, user_id: Int, date: Option[Timestamp], title: String, content: String) extends ActiveRecord

case class BbsData(user_id: String, title: String, content: String)

object Bbs extends ActiveRecordCompanion[Bbs] with PlayFormSupport[Bbs] {
  // 以下のtimestampReadsとtimestampWritesはTimestamp使うために必要
  implicit val timestampReads: Reads[Timestamp] = {
    implicitly[Reads[Long]].map(new Timestamp(_))
  }

  implicit val timestampWrites: Writes[Timestamp] = {
    implicitly[Writes[Long]].contramap(_.getTime)
  }

  implicit val jsonWrites: OWrites[Bbs] = Json.writes[Bbs]
  implicit val jsonReads: Reads[Bbs] = Json.reads[Bbs]
}