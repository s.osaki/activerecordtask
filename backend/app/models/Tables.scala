package models

import com.github.aselab.activerecord._
import com.github.aselab.activerecord.dsl._

object Tables extends ActiveRecordTables with PlaySupport{
  val bbs = table[Bbs]
  // You can also specify its table name. Default is the same name as its class.
  // val people = table[Person]("other_table_name")
}