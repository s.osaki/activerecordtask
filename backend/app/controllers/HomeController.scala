package controllers

import javax.inject._
import play.api.mvc._
import models._
import play.api.libs.json._
import com.github.aselab.activerecord.dsl._
import java.sql.Timestamp

@Singleton
class HomeController @Inject()(val cc: ControllerComponents) extends AbstractController(cc) {
  def index() = Action { implicit request =>
    Ok("hello world")
  }

  def list(): Action[AnyContent] = Action { implicit request =>
    val data = Bbs.all.toList
    Ok(Json.toJson(data))
  }

  /*
  curl http://localhost:9000/bbs -X POST -H 'Content-Type: application/json' -d '{"id": 0, "user_id": 33, "title": "test", "content": "sample data"}'
   */
  def registerBbs(): Action[JsValue] = Action(parse.json) { implicit request =>
    request.body.validate[Bbs].map{ x: Bbs =>
      val timestamp = new Timestamp(System.currentTimeMillis())
      val bbs = x.copy(date = Some(timestamp))
      //println(bbs)
      val check = bbs.save()
      if (check) {
        Ok(Json.toJson(bbs))
      } else {
        InternalServerError(Json.obj("error" -> "database error"))
      }
    }.getOrElse(BadRequest(Json.obj("error" -> "invalid parameters")))
  }
}
